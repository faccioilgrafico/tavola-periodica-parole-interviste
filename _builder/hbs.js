handlebars = require('handlebars');
var fs = require('fs');
var request = require("request");
var moment = require('moment');
var iconv = require('iconv-lite');
var mkdirp = require('mkdirp');
when = require("promised-io/promise").when;


require("./handlebar/helpers.js");


// var url = 'http://corriere.s3.amazonaws.com/json-table/11-settembre.json';
//var url = 'http://corriere.s3.amazonaws.com/json-table/biden-donne.json';
var url = 'https://js.corriereobjects.it/infografiche/json-table/interviste.json';
url+="?ie="+(new Date()).getTime();


request({
    headers: {
      'User-Agent': 'request',
      'Cache-Control': 'private, no-cache, no-store, must-revalidate',
      'Expires': '-1',
      'Pragma': 'no-cache'
    },
    url: url,
    json: true
}, function (error, response, body) {
// test del push to deploy su ftp
    if (!error && response.statusCode === 200) {
        // console.log(body) // Print the json response

        //qui mi gestisco il file
        var attributes = [];

        for(var attr in body){
          attributes.push(attr);
        }

        //Leggo tutti i setting dello speciale
        _settings = body.settings[0];

        //leggo i filtri delo speciale
        filtri = body.filtri;
        _filtri = {};
        for (var i = 0, len = filtri.length; i < len; i++) {
        
          Object.keys(filtri[i]).map(function(objectKey, index) {
            if( !_filtri[objectKey] ){ _filtri[objectKey]  = []; }
            if( filtri[i][objectKey] ){
              _filtri[objectKey].push( filtri[i][objectKey] );
            }
          });
          
        }
        
        /*
        #PAGE LAYOUT
        
        ##Single
        Ogni speciale avrà una pagina index che verrà costruita includendo, 
        nell'ordine specificato dal campo mainpage_disp tutti template.
        Ogni foglio del documento, se ha un template associato, da origine ad un html che puù venire incluso

        ##Multiple
        Se specificato nei settings, vengono create tante pagine 
        quante sono le righe del foglio specificato.
        Tutte le pagine hanno lo steso temoplate e si riferiscono agli stessi dati.
        Per ora
        */

        //Crea la pagina index, Riferirsi alla funzione per i commenti più specifici
        createIndex(_settings); 

        //anche solo con la index devo conoscere 

        //crea tutti gli includes
        attributes.forEach(function(attr){

          fs.readFile('templates/' + attr + '.hbs', function(err, data){
            if (!err) {


              if ( undefined == _settings.multipage_folder || "FALSE" == _settings.multipage_folder){
                _path = "page";
              } else {
                _path = _settings.multipage_folder.split(",");
              }
              // make the buffer into a string
              var source = data.toString();
              // call the render function
              renderToString(source, {
                "data" :  body[attr],
                "path" : "../" + _path,
                "filename" : _settings.multipage_name,
                "filtri": _filtri
              } , '../include-' + attr + '.html');

              console.log("Template", attr, "compilato");

            } else {
              console.log("Non c'è il template", attr);
            }
          });
        });
        
        if( "FALSE" == _settings.single ){
          //multiple pages layout
          console.log("Pagina Multipla");
          console.log("Foglio guida per le pagine interne:", _settings.multipage_sheet )
          //creare tutte le pagine interne
          createInternalPages(body, _settings);
        } else {
          console.log("Pagina Singola");
        }

    }
});


function createIndex(config){

  //leggo la sequenza dei template da inserire nell'index
  //e scrivo l'index con la sequenza corretta
  fs.readFile('templates/index.hbs', function(err, data){
    if (!err) {
      // make the buffer into a string
      var source = data.toString();
      // call the render function
      renderToString(source,
        {
          "testata": config.testata,
          "include": config.corriere_include,
          "ordine" : config.mainpage_disp.replace(/ /g,'').split(",")
        }
        , '../index.shtml');
        return "template index compilato";
      } else {
        return "FATAL ERROR: non c'è il template index";
      }
    });

}

var createInternalPages = function(data, config) { // <-- no callback here
  console.log("internal");
  path = [];
  _filename = [];
  // ogni riga del foglio definito dal config
  totalPages = data[config.multipage_sheet].length;
  
  data[config.multipage_sheet].forEach(function(singola, index){
    /* calcola tutte le pagine e i filename */
    path[index] = getPath(config.multipage_folder, singola);
    _filename[index] = slugify(singola[config.multipage_name]) + '.shtml' ;

    });

  data[config.multipage_sheet].forEach(function(singola, index){
    
    fs.readFile('templates/internal.hbs', function(err, data){
      if (!err) {
        // make the buffer into a string
        var source = data.toString();
        // call the render function

        //crea la cartella se non esiste il campo tiene la root
       

        mkdirp(path[index], function (err) {
          if (err){
            
            console.error("err", err);

          } else {
            
            when( renderToString(source,
              {
                "dati": singola,
                "include": config.corriere_include,
                "ordine" : config.multipage_disp.replace(/ /g,'').split(","),
                "next": _filename[index+1],
                "prev": _filename[index-1],
                "first" : (index == 0),
                "last" : (index == totalPages - 1)
              }
              , path[index] +_filename[index]) , function(result){
                console.log('File', _filename[index] ,'creato correttamente', result);
              },
              function(error){
                 console.log(error);
              });
            
          }
        });

      } else {

        return "FATAL ERROR: non c'è il template index";

      }
    });
  });

}



// this will be called after the file is read
function renderToString(source, data, filename) {
  var template = handlebars.compile(source);
  var outputString = template(data);
  apri_hanldetemple = '';
  chiudi_hanldetemple = '';
  try {
    fs.writeFileSync(filename, "");
    fs.appendFileSync(filename, apri_hanldetemple);
    fs.appendFileSync(filename, outputString);
    fs.appendFileSync(filename, chiudi_hanldetemple);
  }
  catch(err) {
    //Block of code to handle errors
    return err;
  }

  return true;
}


function sortObject(obj, testVal) {
    var arr = [];
    var prop;

    for (prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop][testVal]
            });
        }
    }

    arr.sort(function(a, b) {
        if (a.value > b.value) {
          return 1;
        }
        if (a.value < b.value) {
          return -1;
        }
        // a must be equal to b
        return 0;
    });


    return arr; // returns array
}

function getPath(lista, dati){
  out = ".";

  if ( "FALSE" == lista){
    out = "../page/";
  } else if( undefined != lista){
    out = "../";
    pathTemplate = lista.split(',');
    pathTemplate.forEach(function(cart){
      out += dati[cart] + "/";
    });

  }

  return out;
}



function slugify(text)
{ 
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}